<?php

use yii\db\Migration;
use yii\db\Schema;

class m170322_151736_formdata extends Migration
{
    public function up()
    {
        $this->createTable('form_data', [
            'id' => $this->primaryKey(),
            'email' => $this->string(50)->notNull(),
            'name' => $this->string(50)->notNull(),
            'text' => $this->text()->notNull(),
            'date' => $this->timestamp()->notNull(),
            'url' => $this->string(50),
            'ip' => $this->string(50),
            'browser' => $this->string(50),
            'files' => $this->string(50),
        ]);
    }

    public function down()
    {
        $this->dropTable('form_data');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
