<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_data".
 *
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property string $url
 * @property string $text
 * @property string $ip
 * @property string $browser
 * @property string $files
 * @property string $date
 */
class FormData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name', 'text'], 'required'],
            [['text'], 'string'],
            [['date'], 'safe'],
            [['email', 'name', 'url', 'ip', 'browser', 'files'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'name' => 'Name',
            'url' => 'Url',
            'text' => 'Text',
            'ip' => 'Ip',
            'browser' => 'Browser',
            'files' => 'Files',
            'date' => 'Date',
        ];
    }
}
