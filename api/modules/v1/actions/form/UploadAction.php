<?php
/**
 * Created by PhpStorm.
 * User: Medved
 * Date: 22.03.2017
 * Time: 11:44
 */

namespace app\modules\v1\actions\form;


use yii\rest\Action;
use Yii;
use yii\web\UploadedFile;
use app\models\UploadForm;

/**
 * Class UploadAction
 * @package app\modules\v1\actions\form
 */
class UploadAction extends Action
{
    /**
     * @var string
     */
    public $modelClass = '';

    /**
     *
     */
    public function run()
    {
        if (isset($_FILES['upload_file'])) {
            if (move_uploaded_file($_FILES['upload_file']['tmp_name'], "../../../../app/img/" . $_FILES['upload_file']['name'])) {
                return $_FILES['upload_file']['name'] . " OK";
            } else {
                return $_FILES['upload_file']['name'] . " KO";
            }
        } else {
            return "No files uploaded ...";
        }
    }
}