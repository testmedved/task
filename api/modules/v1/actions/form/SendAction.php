<?php

namespace app\modules\v1\actions\form;

use app\modules\v1\models\FormDataModel;
use yii\rest\Action;
use Yii;


/**
 * Class SendAction
 * @package app\modules\v1\actions\form
 */
class SendAction extends Action
{
    /**
     * @var string
     */
    public $modelClass = '';

    /**
     * @ip Get user ip from yii\web\Request
     * @data Get data from site
     * @return FormDataModel
     */
    public function run()
    {
        $request = Yii::$app->request;
        $ip = $request->getUserIP();                //User IP
        $data = new FormDataModel();
        $data->email = $request->post('email');     //Email
        $data->name = $request->post('name');       //User Name
        $data->url = $request->post('url');         //Homepage
        $data->text = $request->post('text');       //Text comment
        $data->browser = $request->post('browser'); //User browser
        $data->ip = $ip;           //User IP
        if ($data->email != "" && $data->name != "" && $data->text != ""){  //
            if ($data->save()){     //Save data in to DB
                return true;
            }else{
                return false;
            };
        }else{
            return false;
        }
    }

}