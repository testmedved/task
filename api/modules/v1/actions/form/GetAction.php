<?php
/**
 * Created by PhpStorm.
 * User: Medved
 * Date: 22.03.2017
 * Time: 5:39
 */

namespace app\modules\v1\actions\form;


use app\modules\v1\models\FormDataModel;
use yii\rest\Action;


/**
 * Class GetAction
 * @package app\modules\v1\actions\form
 */
class GetAction extends Action
{
    /**
     * @var string
     */
    public $modelClass = '';

    /**
     * Return array of data for comments
     * @return array|\yii\db\ActiveRecord[]
     */
    public function run(){
        return FormDataModel::getData();
    }
}