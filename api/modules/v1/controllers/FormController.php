<?php

namespace app\modules\v1\controllers;


use app\modules\v1\actions\form\GetAction;
use app\modules\v1\actions\form\SendAction;
use app\modules\v1\actions\form\UploadAction;
use yii\rest\ActiveController;

/**
 * Class FormController
 * @package app\modules\v1\controllers
 */
class FormController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = '';

    /**
     * @return array of Actions
     */
    public function actions()
    {
        return [
            //Send comments to databese from site
            'send' => [
                'class' => SendAction::className()
            ],
            // Get comments from database
            'get' => [
                'class' => GetAction::className()
            ],
            //Upload files from site
            'upload' => [
                'class' => UploadAction::className()
            ]
        ];
    }
}