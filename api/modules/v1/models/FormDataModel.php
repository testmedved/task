<?php

namespace app\modules\v1\models;
use app\models\FormData;

/**
 * Class FormDataModel
 * @package app\modules\v1\models
 */
class FormDataModel extends FormData
{
    /**
     * Select and return from database
     * @name Text User Name
     * @email Email
     * @date DateTime Date and time of comment
     * @text text Text of comment
     * @extend integer Extend comment
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getData(){
        return self::find()
            ->select(['id', 'name', 'email', 'date', 'text', 'extend', 'extended'])
            ->all();
    }
}