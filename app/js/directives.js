app.directive('commentModal', function () {
    return{
        templateUrl: 'app/templates/form.html',
        link: function () {
        }
    };
});

app.directive('commentExtend', function () {
    return{
        templateUrl: 'app/templates/comment.html',
        scope:{
            dataScope:'@dataScope',
            id:'@id'
        },
        link: function () {
        }
    };
});