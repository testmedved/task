app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            title : 'Форма',
            templateUrl:'app/templates/form.html'
        })
        .when('/result', {
            title : 'Результат',
            templateUrl:'app/templates/result.html'
        })
        .when('/comments', {
            title : 'Комментарии',
            templateUrl:'app/templates/comments.html'
        })
        .otherwise({
            title : 'Форма',
            redirectTo: '/'
        });
    $locationProvider.html5Mode(true).hashPrefix('!');
});

