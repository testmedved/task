app.controller("FormCtrl", function ($scope, $sce, $http) {
    $scope.email = '';
    $scope.name = '';
    $scope.url = '';
    $scope.text = '';
    if (navigator.userAgent.search("MSIE") >= 0) {
        $scope.browser = "InternetExplorer";
    }
    //Check if browser is Chrome or not
    else if (navigator.userAgent.search("Chrome") >= 0) {
        $scope.browser = "Chrome";
    }
    //Check if browser is Firefox or not
    else if (navigator.userAgent.search("Firefox") >= 0) {
        $scope.browser = "FireFox";
    }
    //Check if browser is Safari or not
    else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
        $scope.browser = "Safari";
    }
    //Check if browser is Opera or not
    else if (navigator.userAgent.search("Opera") >= 0) {
        $scope.browser = "Opera";
    }

    $scope.send_mail = function () {
        if ($scope.email !== '' && $scope.name !== '' && $scope.text !== ''){
            $http.post('api/v1/form/send', {
                    "email": $scope.email,
                    "name": $scope.name,
                    "url": $scope.url,
                    "text": $scope.text,
                    "browser": $scope.browser
                }, {headers:{'Content-Type': 'application/json'}}
            ).then(function successCallback(DataPage){
                console.log(DataPage);
            }, function errorCallback(DataPage){
                console.log(DataPage);
            })}



    };

    (function(){
        function previewImage(file) {
            var galleryId = "gallery";

            var gallery = document.getElementById(galleryId);
            var imageType = /image.*/;

            if (!file.type.match(imageType)) {
                throw "File Type must be an image or txt";
            }

            var thumb = document.createElement("div");
            thumb.classList.add('thumbnail');

            var img = document.createElement("img");
            img.file = file;
            thumb.appendChild(img);
            gallery.appendChild(thumb);

            // Using FileReader to display the image content
            var reader = new FileReader();
            reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
            reader.readAsDataURL(file);
        }

        var uploadfiles = document.querySelector('#fileinput');
        uploadfiles.addEventListener('change', function () {
            var files = this.files;
            for(var i=0; i<files.length; i++){
                previewImage(this.files[i]);
            }

        }, false);

        function uploadFile(file){
            var url = 'app/templates/upload.php';
            // var url = 'api/v1/form/upload';
            var xhr = new XMLHttpRequest();
            var fd = new FormData();
            xhr.open("POST", url, true);
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    // Every thing ok, file uploaded
                    console.log(xhr.responseText); // handle response.
                }
            };
            fd.append("upload_file", file);
            xhr.send(fd);
        }

        var uploadfiles1 = document.querySelector('#fileinput');
        uploadfiles1.addEventListener('change', function () {
            var files = this.files;
            console.log(files);
            for(var i=0; i<files.length; i++){
                if (this.files[i].type === "image/png" ||this.files[i].type === "image/jpg" || this.files[i].type === "image/gif") {
                    uploadFile(this.files[i]); // call the function to upload the file
                }
            }
        }, false);

    })();

});