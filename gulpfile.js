var gulp = require('gulp');
var inject = require('gulp-inject');

gulp.task('default', function(){

    var target = gulp.src('./index.html');

    var libs = {
        js:[
            './node_modules/angular/angular.js',
            './node_modules/angular-route/angular-route.min.js',
            './node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js',
            './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
            './node_modules/angular-utils-pagination/dirPagination.js',
            './node_modules/angular-recaptcha/release/angular-recaptcha.min.js',
            './node_modules/jquery/dist/jquery.min.js',
            './node_modules/bootstrap/dist/js/bootstrap.min.js',
            './node_modules/ui-comments/src/comments.js'
        ],
        css:[
            './node_modules/bootstrap/dist/css/bootstrap.min.css',
            './node_modules/bootstrap/dist/css/bootstrap-theme.min.css'
            // './node_modules/angular-bootstrap-file-upload-master/dist/angular-bootstrap-file-upload.min.css'
        ]
    };

    return target
        .pipe(inject( gulp.src(['./app/js/*.js', './app/css/*.css'], {read: false}), {addRootSlash: false} ))
        .pipe(inject( gulp.src(libs.js, {read: false}), {name: 'libs', addRootSlash: false} ))
        .pipe(inject( gulp.src(libs.css, {read: false}), {name: 'libs', addRootSlash: false} ))
        .pipe(gulp.dest('./'));

});